/* eslint-env node */
module.exports = {
  extends: [
    'eslint:recommended', 
    'plugin:@typescript-eslint/recommended', 
    "plugin:security/recommended-legacy",
    "plugin:sonarjs/recommended"
  ],
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint', 
    'security',
    'sonarjs'
  ],
  root: true,
};