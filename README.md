![GitHub package.json version](https://img.shields.io/github/package-json/v/segen-des-tages/api)
![Discord](https://img.shields.io/discord/1212765882065883136)
![GitHub License](https://img.shields.io/github/license/segen-des-tages/api)
![GitHub last commit](https://img.shields.io/github/last-commit/segen-des-tages/api)
![build status docker](https://github.com/Segen-des-Tages/api/actions/workflows/docker-publlsh.yml/badge.svg)
![build status server](https://github.com/Segen-des-Tages/api/actions/workflows/server-deploy.yml/badge.svg)
![GitHub issues](https://img.shields.io/github/issues-raw/segen-des-tages/api)
[![codecov](https://codecov.io/gh/Segen-des-Tages/api/graph/badge.svg?token=F2SZDSVU9K)](https://codecov.io/gh/Segen-des-Tages/api)

# Segen-des-Tages / API

A Simple REST-API to get blessings of the day, the calendar or all blessings

## Installation

There are several ways to install the API. The recommended method is to use Docker, but you can also install it manually or just use the version deployed on <https://api.segen-des.tages.de>.

### Docker / Podman

1. Install Docker or Podman
2. Pull the database image from the registry: `docker pull ghcr.io/segen-des-tages/database:latest` or `podman pull ghcr.io/segen-des-tages/database:latest`
3. Pull the image from the registry: `docker pull ghcr.io/segen-des-tages/api:latest` or `podman pull ghcr.io/segen-des-tages/api:latest`
4. Run the Images
5. Database: `docker run -p 3306:3306 ghcr.io/segen-des-tages/database:latest` or `podman run -p 3306:3306 ghcr.io/segen-des-tages/database:latest`
6. API: `docker run -p 3000:3000 ghcr.io/segen-des-tages/api:latest` or `podman run -p 3000:3000 ghcr.io/segen-des-tages/api:latest`

### Manual Installation

1. Install node.js and npm
2. Clone the repository: `git clone https://github.com/Segen-des-Tages/api.git`
3. Install the dependencies: `npm install`
4. Run the API: `npm run serve`
5. Direct your browser to `http://localhost:3000/`

## Usage

The easiest way to use the API is to use the Swagger UI at `http://localhost:3000/_api` or `https://api.segen-des-tages.de/_api`.

## Contributing

We appreciate and welcome contributions from the community to enhance the features and overall quality of Segen des Tages. Whether you're a developer, tester, or enthusiastic user, there are several ways you can contribute:

### Creating Issues

If you encounter a bug, have a feature request, or want to suggest improvements, please [create an issue](https://github.com/Segen-des-Tages/api/issues/new) on our Github repository. When creating an issue, provide detailed information about the problem or enhancement you're addressing. This includes steps to reproduce the issue and any relevant context that can help our team understand and address it effectively.

### Pull Requests

If you'd like to contribute code, documentation, or fixes, we encourage you to submit a pull request. Before creating a pull request, please:

1. Fork the repository.
2. Create a new branch for your changes.
3. Make your modifications, ensuring adherence to our coding standards.
4. Write tests for new features or modifications.
5. Ensure all tests pass.
6. Submit a pull request to the `main` branch of the repository.

We'll review your pull request, provide feedback, and work with you to ensure that your contribution aligns with the project's goals and standards.


## Tech Stack

segen-des-tages/api is built on the following main stack:

- <img width='25' height='25' src='https://img.stackshare.io/service/1612/bynNY5dJ.jpg' alt='TypeScript'/> [TypeScript](http://www.typescriptlang.org) – Languages
- <img width='25' height='25' src='https://img.stackshare.io/service/1209/javascript.jpeg' alt='JavaScript'/> [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) – Languages
- <img width='25' height='25' src='https://img.stackshare.io/service/1163/hashtag.png' alt='ExpressJS'/> [ExpressJS](http://expressjs.com/) – Microframeworks (Backend)
- <img width='25' height='25' src='https://img.stackshare.io/service/1011/n1JRsFeB_400x400.png' alt='Node.js'/> [Node.js](http://nodejs.org/) – Frameworks (Full Stack)
- <img width='25' height='25' src='https://img.stackshare.io/service/2416/default_274ed9bcc98502018f007cfcd8c09f3514d3d319.png' alt='Cron'/> [Cron](https://directory.fsf.org/wiki/Cron) – Background Processing
- <img width='25' height='25' src='https://img.stackshare.io/service/1025/logo-mysql-170x170.png' alt='MySQL'/> [MySQL](http://www.mysql.com) – Databases
- <img width='25' height='25' src='https://img.stackshare.io/service/586/n4u37v9t_400x400.png' alt='Docker'/> [Docker](https://www.docker.com/) – Virtual Machine Platforms & Containers
- <img width='25' height='25' src='https://img.stackshare.io/service/3337/Q4L7Jncy.jpg' alt='ESLint'/> [ESLint](http://eslint.org/) – Code Review
- <img width='25' height='25' src='https://img.stackshare.io/service/11563/actions.png' alt='GitHub Actions'/> [GitHub Actions](https://github.com/features/actions) – Continuous Integration
- <img width='25' height='25' src='https://img.stackshare.io/service/830/jest.png' alt='Jest'/> [Jest](http://facebook.github.io/jest/) – Javascript Testing Framework
- <img width='25' height='25' src='https://img.stackshare.io/no-img-open-source.png' alt='SuperTest'/> [SuperTest](https://www.npmjs.com/package/supertest) – Javascript Testing Framework
- <img width='25' height='25' src='https://img.stackshare.io/service/4631/default_c2062d40130562bdc836c13dbca02d318205a962.png' alt='Shell'/> [Shell](https://en.wikipedia.org/wiki/Shell_script) – Shells

Full tech stack [here](/techstack.md)



## License

Segen des Tages API is licensed under the [MIT License](LICENSE.md). Feel free to use, modify, and distribute it according to your needs.

## Authors

- **[Dominik Sigmund](https://github.com/DSigmund)** - Lead Designer and Developer

## Acknowledgments

We appreciate the collaborative efforts that have contributed to the success of Segen des Tages API. We'd like to thank the following individuals and organizations for their support and contributions:

- (none yet, but you could be the first!)

## Support

If you can't find a solution to your issue in the documentation, feel free to reach out to us for assistance. We offer support through the following channels:

- **Discord:** Join our [Discord server](https://discord.gg/5qXGRWUy) to connect with the community and get help from the Segen-des-Tages team.

- **Email:** For non-urgent matters or if you prefer email communication, you can reach us at [api@segen-des-tages.de](mailto:api@segen-des-tages.de). Please provide detailed information about your issue so that we can assist you more effectively.

## Important Note

When seeking support, make sure to include the following information in your message:

1. A detailed description of the issue you're facing.
2. Steps to reproduce the problem (if applicable).
3. Relevant configuration details.
4. Any error messages or logs related to the issue.

This information will help us understand your situation better and provide a more accurate and timely response.

Thank you for choosing Segen des Tages!! We're committed to ensuring you have a positive experience, and we appreciate your cooperation in following these support guidelines.