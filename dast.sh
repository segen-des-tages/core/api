#!/bin/bash

PORT=3000
if [ -n "$1" ]; then
  PORT=$1
fi

CONTAINERCLI=podman
if ! command -v $CONTAINERCLI &> /dev/null
then
  CONTAINERCLI=docker
fi

LOCALFOLDER=$(pwd)/reports/dast/

# Step 0: Pull the OWASP ZAP Docker image
$CONTAINERCLI pull owasp/zap2docker-stable

# Step 1: Start the server
npm run build && npm start &

# Step 2: Wait for the server to be ready
while ! curl http://localhost:$PORT/_health --silent; do sleep 1; done

# Step 3: Run OWASP ZAP scan
$CONTAINERCLI run --rm -v $LOCALFOLDER:/zap/wrk/:rw -t owasp/zap2docker-stable zap-baseline.py -t http://localhost:$PORT -J report.json -r report.html

# Step 5: Tear down the server
kill $(lsof -t -i:$PORT)

