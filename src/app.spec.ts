import request from 'supertest';
import { Application } from './app';
import { Log } from '@libs/log';
import { createConfig, Config } from '@libs/config'
import { MockDatabase } from './libs/controller/__mocks__/database';
import { BlessingsTable, CalendarTable, LanguagesTable } from './libs/controller/database';

const mockLogger: Log = {
  log: jest.fn(),
  error: jest.fn(),
  info: jest.fn(),
  notice: jest.fn(),
  debug: jest.fn(),
  warning: jest.fn()
};

const mockDatabase = new MockDatabase();

const today = new Date();
const date_1 = new Date(2021, 1, 1, 12);
const date_2 = new Date(2021, 2, 2, 12);

describe('Application', () => {
  let application: Application;

  beforeAll(async () => {
    const config: Config = createConfig(__dirname + '/../');

    mockDatabase.setQueryResult(
      "SELECT id, blessing, language FROM blessings",
      [
        {"id": "1", "blessing": "Blessing 1", "language": "de"} as BlessingsTable,
        {"id": "2", "blessing": "Blessing 2", "language": "en"} as BlessingsTable,
        {"id": "3", "blessing": "Blessing 3", "language": "de"} as BlessingsTable,
      ]
    );
    mockDatabase.setQueryResult(
      "SELECT id, date, blessing, language FROM calendar",
      [
        {"id": "1", "date": date_1, "blessing": "1", "language": "de"} as CalendarTable,
        {"id": "2", "date": date_2, "blessing": "2", "language": "en"} as CalendarTable,
        {"id": "3", "date": today, "blessing": "3", "language": "de"} as CalendarTable,
      ]
    );
    mockDatabase.setQueryResult(
      "SELECT `key`, name, active FROM languages",
      [
        {"key": "en", "name": "English", "active": true} as LanguagesTable,
        {"key": "es", "name": "Spanish", "active": true} as LanguagesTable,
        {"key": "fr", "name": "French", "active": false} as LanguagesTable,
      ]
    )
    application = new Application(mockLogger, config, mockDatabase);
    await application.initialize();
  });

  it("should create an instance of Application", () => {
    expect(application).toBeInstanceOf(Application);
  });

  it("GET / should return 200 and the title page", async () => {
    const response = await request(application.getExpressApp()).get('/');
    expect(response.status).toBe(200);
    expect(response.text).toContain('<h2>Todays blessing</h2>');
  });

  it("GET /favicon.ico should return 204", async () => {
    const response = await request(application.getExpressApp()).get('/favicon.ico');
    expect(response.status).toBe(204);
  });

  it("GET /_health should return 200", async () => {
    const response = await request(application.getExpressApp()).get('/_health');
    expect(response.status).toBe(200);
  });
  

  it("GET /languages should return 200 and the list of languages", async () => {
    const response = await request(application.getExpressApp()).get('/languages');
    expect(response.status).toBe(200);
    expect(response.body).toEqual([
      {"key": "en", "name": "English", "active": true},
      {"key": "es", "name": "Spanish", "active": true},
      {"key": "fr", "name": "French", "active": false},
    ]);
  });

  it("GET /calendar/today/de should return 200 and the blessing for today in German", async () => {
    const response = await request(application.getExpressApp()).get('/calendar/today/de');
    expect(response.status).toBe(200);
    expect(response.body.blessing).toBe("Blessing 3");
  });

  it("GET /asdfg should return 404", async () => {
    const response = await request(application.getExpressApp()).get('/asdfg');
    expect(response.status).toBe(404);
  });
  // Add more tests as necessary
});
