import express, { Express } from 'express';
import cors from 'cors';
import * as packageInfo from '../package.json';

import expressJSDocSwagger from 'express-jsdoc-swagger';

import metaRoutes from '@libs/metaroutes';

import { DatabaseInterface } from './libs/controller/database.interface';
import { LanguageController } from './libs/controller/languages';
import { LanguageRoutes } from './libs/routes/languages'; 
import { BlessingController } from './libs/controller/blessings';
import { BlessingsRoutes } from './libs/routes/blessings';
import { CalendarController } from './libs/controller/calendar';
import { CalendarRoutes } from './libs/routes/calendar';
import { Log } from '@libs/log';
import { Config } from '@libs/config'
import Security = require("@libs/security")
import { Metrics, } from '@libs/metrics'

import * as cron from 'node-cron';
import cronParser from 'cron-parser';


export class Application {
  private app: Express;
  private database!: DatabaseInterface;
  private languageController!: LanguageController;
  private languageRoutes!: LanguageRoutes;
  private blessingController!: BlessingController;
  private blessingRoutes!: BlessingsRoutes;
  private calendarController!: CalendarController;
  private calendarRoutes!: CalendarRoutes;
  private logger: Log;
  private config: Config;
  private security: Security;
  private metrics: Metrics;
  private _version: string;
  private _name: string;
  private _description: string;
  private _homepage: string;

  constructor(logger: Log, config: Config, database: DatabaseInterface) {
    this.app = express();
    this.config = config;
    this.logger = logger;
    this.database = database;
    this.security = new Security(this.config.security)
    this.metrics = new Metrics(this.config.metrics);
    this._version = packageInfo.version;
    this._name = packageInfo.name;
    this._description = packageInfo.description;
    this._homepage = packageInfo.homepage;
  }
  public async initialize() {
      this.logger.info('Initializing application');
      this.initializeMiddleware();
      await this.initializeControllers();
      this.initializeRoutes();
      this.initializeErrorHandling();
      this.app.listen(this.config.port, () => {
        this.logger.info(`Server running on port ${this.config.port}`);
        this.startReloadCron(this.config.reloadCron);
      });
  }
  private initializeMiddleware() {
    this.logger.info('Initializing middleware');
    this.app.use(cors());
    this.app.set('etag', 'strong');
    this.app.use(express.json());
    this.app.use(this.security.setHeaders);
    this.app.use('*', this.metrics.collect)
  }
  private async initializeControllers() {
    this.logger.info('Initializing controllers');
    this.languageController = new LanguageController(this.database);
    this.blessingController = new BlessingController(this.database);
    this.calendarController = new CalendarController(this.database);
    await this.loadControllerData();
  }
  private async loadControllerData() {
    await this.languageController.load();
    this.logger.debug('Languages loaded, count: ' + this.languageController.languages.length);
    await this.blessingController.load();
    this.logger.debug('Blessings loaded, count: ' + this.blessingController.blessings.length);
    await this.calendarController.load();
    this.logger.debug('Calendar loaded, count: ' + this.calendarController.calendar.length);
    this.logger.debug('German Blessing for today: ' + this.calendarController.getBlessingIdForTodayByLanguage("de"));
  }
  private initializeRoutes() {
    this.logger.info('Initializing routes');
    this.initializeFaviconRoute();
    this.initializeRootRoute();
    this.initializeMetaRoutes();
    this.initializeAPIRoute();
    this.languageRoutes = new LanguageRoutes(this.languageController);
    this.app.use('/languages', this.languageRoutes.router);
    this.blessingRoutes = new BlessingsRoutes(this.blessingController);
    this.app.use('/blessings', this.blessingRoutes.router);
    this.calendarRoutes = new CalendarRoutes(this.calendarController, this.blessingController);
    this.app.use('/calendar', this.calendarRoutes.router);
  }
  private initializeFaviconRoute() {
    this.logger.info('Initializing favicon route');
    this.app.get('/favicon.ico', (req, res) => {
      res.status(204).end();
    });
  }
  private initializeRootRoute() {
    this.logger.info('Initializing root route');
    const todaysBlessingID = this.calendarController.getBlessingIdForTodayByLanguage("de");
    const todaysBlessing = this.blessingController.getBlessing(todaysBlessingID);
    const html = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${this._name} v${this._version}</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #f5f5f5;
                color: #333;
            }
            h1, h2 {
                color: #0073e6;
            }
            p {
                font-size: 16px;
                line-height: 1.6;
            }
            a {
                color: #0073e6;
                text-decoration: none;
            }
            a:hover {
                text-decoration: underline;
            }
            .container {
                background-color: #fff;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 2px 4px rgba(0,0,0,0.1);
            }
            .github-fork-ribbon {
              width: 150px;
              position: absolute;
              top: 0;
              right: 0;
              z-index: 1000;
              overflow: hidden;
              height: 150px;
              text-align: center;
          }
          .github-fork-ribbon a {
              width: 200px;
              position: absolute;
              top: 43px;
              right: -43px;
              transform: rotate(45deg);
              -webkit-transform: rotate(45deg);
              text-align: center;
              color: #fff;
              font-size: 13px;
              background-color: #000;
              text-decoration: none;
              font-weight: bold;
              line-height: 2em;
          }
        </style>
    </head>
    <body>
        <div class="github-fork-ribbon">
          <a href="https://github.com/Segen-des-Tages/api" target="_blank">Fork me on GitHub</a>
        </div>
        <div class="container">
          <h1>${this._name} v${this._version}</h1>
            <p>${this._description}</p>
          <h2>Todays blessing</h2>
            <p>${todaysBlessing.blessing}</p>
          <h2>Stats</h2>
            <p>Number of blessings: ${this.blessingController.blessings.length}</p>
            <p>Number of active languages: ${this.languageController.getActiveLanguages().length}</p>
          <h2>Links</h2>
            <ul>
              <li><a href="/_api">Full API Documentation</a></li>
              <li><a href="/calendar/today/de">Todays blessing in German: /calendar/today/de</a></li>
            </ul>
        </div>
    </body>
    </html>
    `;
    this.app.get('/', (req, res) => {
      res.send(html);
    });
  }
  private initializeMetaRoutes() {
    this.logger.info('Initializing meta routes');
    metaRoutes(
      this.app, 
      { 
        version: this._version, 
        config: this.config, 
        isServiceReady: this.database.isReady, 
        metricsEndpoint: this.metrics.endpoint 
      }, 
      this.config.metaroutes); 
  }
  private initializeAPIRoute() {
    this.logger.info('Initializing API route');
    const settings = this.config.openapi;
    settings.info.title = this._name;
    settings.info.version = this._version;
    settings.info.description = this._description + ' <br/><a href="' + this._homepage + '" target="_blank">' + this._homepage + '</a>';
    settings.baseDir = __dirname;
    expressJSDocSwagger(this.app)(settings)
  }
  private initializeErrorHandling(): void {
    this.logger.info('Initializing error handling');
    this.app.use((req, res) => {
      res.status(404).send('404 - Not found');
    });
  }
  private startReloadCron(cronString: string): void {
    const task = cron.schedule(cronString, async () => {
      this.logger.info("Reloading Database data as per cron " + cronString)
      await this.loadControllerData();
      try {
        const interval = cronParser.parseExpression(cronString);
        this.logger.info(`Next run at: ${interval.next().toString()}`);
      } catch (err) {
        this.logger.error('Error parsing cron string:', err);
      }
    }, {
      scheduled: true
    });
    task.start();
    try {
      const interval = cronParser.parseExpression(cronString);
      this.logger.info("Cronjob for Reloading Database set to " + cronString) 
      this.logger.info(`Next run at: ${interval.next().toString()}`);
    } catch (err) {
      console.error('Error parsing cron string:', err);
    }
  }
  public getExpressApp(): Express {
    return this.app;
  }
}
/**
   * GET /
   * @summary This is the main entry point, it will display some stats
   * @tags meta
   * @return {string} 200 - OK
   */
  /**
   * GET /_version
   * @summary Returns the version of the service
   * @tags meta
   * @return {string} 200 - The version of the service
   */
  /**
   * GET /_config
   * @summary Returns the configuration of the service with redacted secrets
   * @tags meta
   * @return {object} 200 - The configuration of the service
   */
  /**
   * GET /_health
   * @summary Returns the health of the service
   * @description This endpoint is used to check if the service is healthy and should always return 200
   * @tags meta
   * @return {string} 200 - OK
   */
  /**
   * GET /_ready
   * @summary Returns the readiness of the service
   * @description This endpoint is used to check if the service is ready and should return 200 if the service is ready to serve requests
   * @tags meta
   * @return {string} 200 - OK
   * @return {string} 503 - Service Unavailable
   */
  /**
   * GET /_metrics
   * @summary Returns the metrics of the service
   * @description This endpoint is used to return Prometheus metrics of the service
   * @tags meta
   * @return {string} 200 - OK
   * @return {string} 503 - Service Unavailable
   * @return {string} 500 - Internal Server Error
   */