import { Blessing } from "../../models/blessing";
import { BlessingsControllerInterface } from "../blessings.interface";

export class MockBlessingsController implements BlessingsControllerInterface {
  public _provokeError: boolean = false;
  public _blessings: Blessing[] = [];
  private errorMessage:string = 'Database error';
  constructor() {
    return;
  }
  public async load(): Promise<void> {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
  }
  public get blessings(): Blessing[] {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    return this._blessings;
  }
  public getBlessing(id: string): Blessing {
    if (this._provokeError) {
      throw new Error(`Blessing not found: ${id}`);
    } else {
      return this._blessings.find((blessing) => blessing.id === id) || new Blessing('xx', 'xxxx', 'xx');
    }
  }
  public getBlessingsByLanguage(language: string): Blessing[] {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    return this._blessings.filter((blessing) => blessing.languageKey === language);
  }
}