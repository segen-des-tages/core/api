import { CalendarEntry } from '../../models/calendarEntry';
import { CalendarControllerInterface } from '../calendar.interface';

export class MockCalendarController implements CalendarControllerInterface {
  public _provokeError: boolean = false;
  public _calendar: CalendarEntry[] = [];
  private errorMessage:string = 'Database error';
  constructor() {
    return;
  }
  public async load(): Promise<void> {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
  }
  public get calendar(): CalendarEntry[] {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    return this._calendar;
  }
  public getBlessingIdByDateAndLanguage(date: string, language: string): string {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    const entry = this._calendar.find((entry) => entry.date === date && entry.languageKey === language);
    if (!entry) {
      throw new Error(`No entry found for date ${date} and language ${language}`);
    } else {
      return entry.blessingKey;
    }
  }
  public getBlessingIdForTodayByLanguage(language: string): string {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    const today = this._calendar[0].date;
    return this.getBlessingIdByDateAndLanguage(today, language);
  }
  public getCalendarByYear(year: number): CalendarEntry[] {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    return this._calendar.filter((entry) => entry.date.startsWith(year.toString()));
  }
  public getCalendarByLanguage(language: string): CalendarEntry[] {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    return this._calendar.filter((entry) => entry.languageKey === language);
  }
  public getCalendarByLanguageAndYear(language: string, year: number): CalendarEntry[] {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    const entries = this._calendar.filter((entry) => entry.languageKey === language && entry.date.startsWith(year.toString()));
    if (entries.length === 0) {
      throw new Error(`No entries found for language ${language} and year ${year}`);
    } else {
      return entries;
    }
  }
  public getBlessingIdsByDate(date: string): string[] {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    return this._calendar.filter((entry) => entry.date === date).map((entry) => entry.blessingKey);
  }
}