import { DatabaseSettingsInterface } from "../database.settings.interface";

export class MockDatabaseSettings implements DatabaseSettingsInterface {
  host: string;
  user: string;
  password: string;
  database: string;
  port: number;
  connectionLimit: number;
  constructor(host: string, user: string, password: string, database: string, port: number, connectionLimit: number) {
    this.host = host;
    this.user = user;
    this.password = password;
    this.database = database;
    this.port = port;
    this.connectionLimit = connectionLimit;
  }
}