import { RowDataPacket } from "mysql2";
import { DatabaseInterface } from "../database.interface";

export class MockDatabase implements DatabaseInterface {
  private _queryResults: Map<string, RowDataPacket[]> = new Map();
  public _throwError: boolean = false;

  constructor() {}

  public async query<T extends RowDataPacket[]>(sql: string): Promise<T> {
    return new Promise((resolve, reject) => {
      if (this._throwError) {
        reject(new Error("MockDatabase Error"));
      } else {
        const result = this._queryResults.get(sql);
        if (!result) {
          reject(new Error("Query result not found"));
        } else {
          // Ensure the result matches the expected type T[]
          resolve(result as T);
        }
      }
    });
  }

  // Enhance setQueryResult to accept RowDataPacket[] or any array to simulate different types of query results.
  public setQueryResult<T extends RowDataPacket[]>(query: string, result: T): void {
    this._queryResults.set(query, result);
  }

  public async isReady(): Promise<boolean> {
    return !this._throwError;
  }
}
