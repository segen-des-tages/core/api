import { Language } from "../../models/language";
import { LanguagesControllerInterface } from "../languages.interface";

export class MockLanuagesController implements LanguagesControllerInterface {
  public _provokeError: boolean = false;
  public _languages: Language[] = [];
  private errorMessage:string = 'Database error';
  constructor() {
    return;
  }
  public async load(): Promise<void> {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
  }
  public get languages(): Language[] {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    return this._languages;
  }
  public getLanguage(key: string): Language {
    if (this._provokeError) {
      throw new Error(`Language not found: ${key}`);
    } else {
      return this._languages.find((language) => language.key === key) || new Language('xx', 'xxxx', false);
    }
  }
  public getActiveLanguages(): Language[] {
    if (this._provokeError) {
      throw new Error(this.errorMessage);
    }
    return this._languages.filter((language) => language.isActive);
  }
}