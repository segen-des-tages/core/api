import { DatabaseSettingsInterface } from "../../database.settings.interface";

// __mocks__/mysql2/promise.ts or manual jest.mock in your test file
export const mockQuery = jest.fn().mockResolvedValue([[{ id: 1 }], []]); // Mock query result and fields
export const mockRelease = jest.fn().mockResolvedValue(null); // Resolves immediately for releasing the connection
export const mockEnd = jest.fn().mockResolvedValue(null); // Resolves immediately for ending the pool

const mockPoolConnection = {
  query: mockQuery,
  release: mockRelease,
};

export const mockPool = {
  getConnection: jest.fn(() => Promise.resolve(mockPoolConnection)),
  end: mockEnd,
};

// Mocking createPool to return the mockPool object, needs an object as argument
export const createPool = function (settings: DatabaseSettingsInterface) {
  if (!settings) {
    throw new Error("Database settings are required");
  }
  if (!settings.host) {
    throw new Error("Database host is required");
  }
  if (!settings.user) {
    throw new Error("Database user is required");
  }
  if (!settings.password) {
    throw new Error("Database password is required");
  }
  if (!settings.database) {
    throw new Error("Database name is required");
  }
  if (!settings.port) {
    throw new Error("Database port is required");
  }
  if (!settings.connectionLimit) {
    throw new Error("Database connection limit is required");
  }
  return mockPool;
};

// Optional: If you explicitly use Pool class or type checking
export class Pool {
  getConnection = jest.fn(() => Promise.resolve(mockPoolConnection));
  end = mockEnd;
}
