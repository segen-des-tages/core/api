import { Blessing } from "../models/blessing";

export interface BlessingsControllerInterface {
  blessings: Blessing[];
  load(): Promise<void>;
  getBlessing(id: string): Blessing;
  getBlessingsByLanguage(langauge: string): Blessing[];
}