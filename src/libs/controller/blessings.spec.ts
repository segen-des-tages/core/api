import { MockDatabase } from './__mocks__/database';
import { BlessingController } from './blessings';
import { Blessing } from '../models/blessing';
import { BlessingsTable } from './database';

const database = new MockDatabase();

describe('Controller :: Blessings', () => {
  beforeAll(() => {
    database.setQueryResult(
      "SELECT id, blessing, language FROM blessings",
      [
        {"id": "1", "blessing": "Blessing 1", "language": "de"} as BlessingsTable,
        {"id": "2", "blessing": "Blessing 2", "language": "de"} as BlessingsTable,
        {"id": "3", "blessing": "Blessing 3", "language": "en"} as BlessingsTable,
      ]
    )
  });
  it("should create an instance of Blessings", () => {
    const blessings = new BlessingController(database);
    expect(blessings).toBeInstanceOf(BlessingController);
  });
  it("should load the blessings", async () => {
    const blessings = new BlessingController(database);
    await blessings.load();
    expect(blessings.blessings).toHaveLength(3);
    expect(blessings.blessings[0]).toBeInstanceOf(Blessing);
  });
  it("should return a list of blessings", async () => {
    const blessings = new BlessingController(database);
    await blessings.load();
    const list = blessings.blessings;
    expect(list).toHaveLength(3);
    expect(list[0]).toBeInstanceOf(Blessing);
  });
  it("should return a blessing by id", async () => {
    const blessings = new BlessingController(database);
    await blessings.load();
    const blessing = blessings.getBlessing("1");
    expect(blessing).toBeInstanceOf(Blessing);
    expect(blessing?.id).toBe("1");
  });
  it("should throw an error if blessing not found", async () => {
    const blessings = new BlessingController(database);
    await blessings.load();
    expect(() => {
      blessings.getBlessing("xx");
    }).toThrow("Blessing not found: xx");
  });
  it("should return a list of blessings by language", async () => {
    const blessings = new BlessingController(database);
    await blessings.load();
    const list = blessings.getBlessingsByLanguage("de");
    expect(list).toHaveLength(2);
    expect(list[0]).toBeInstanceOf(Blessing);
  });
});