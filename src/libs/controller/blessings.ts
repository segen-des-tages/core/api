import { Blessing } from "../models/blessing";
import { BlessingsControllerInterface } from "./blessings.interface";
import { BlessingsTable } from "./database";
import { DatabaseInterface } from "./database.interface";



export class BlessingController implements BlessingsControllerInterface{
  private _database: DatabaseInterface;
  private _blessings: Blessing[];
  constructor(database: DatabaseInterface) {
    this._database = database;
    this._blessings = [];
  }
  public async load(): Promise<void> {
    const results = await this._database.query<BlessingsTable>('SELECT id, blessing, language FROM blessings');
    this._blessings = results.map((result) => {
      return new Blessing(result.id, result.blessing, result.language);
    });
  }
  public get blessings(): Blessing[] {
    return this._blessings;
  }
  public getBlessing(id: string): Blessing { 
    const found = this._blessings.find((blessing) => blessing.id.toString() === id.toString());
    if (!found) {
      throw new Error(`Blessing not found: ${id}`);
    } else {
      return found;
    }
  }
  public getBlessingsByLanguage(language: string): Blessing[] {
    return this._blessings.filter((blessing) => blessing.languageKey.toString() === language.toString());
  }
}