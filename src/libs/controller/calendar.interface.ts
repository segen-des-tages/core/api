import { CalendarEntry } from "../models/calendarEntry";

export interface CalendarControllerInterface {
  load(): Promise<void>;
  calendar: CalendarEntry[];
  getBlessingIdByDateAndLanguage(date: string, language: string): string;
  getBlessingIdsByDate(date: string): string[];
  getBlessingIdForTodayByLanguage(language: string): string;
  getCalendarByYear(year: number): CalendarEntry[];
  getCalendarByLanguage(language: string): CalendarEntry[];
  getCalendarByLanguageAndYear(language: string, year: number): CalendarEntry[];
}