import { CalendarController } from './calendar';
import { CalendarEntry } from '../models/calendarEntry';
import { MockDatabase } from './__mocks__/database';
import { CalendarTable } from './database';

const database = new MockDatabase();

const today = new Date();
const date_1 = new Date(2021, 1, 1, 12);
const literal_date_1 = "2021-02-01";
const date_2 = new Date(2021, 2, 2, 12);


describe('Controller :: Calendar', () => {
  beforeAll(() => {
    database.setQueryResult(
      "SELECT id, date, blessing, language FROM calendar",
      [
        {"id": "1", "date": date_1, "blessing": "1", "language": "de"} as CalendarTable,
        {"id": "2", "date": date_2, "blessing": "2", "language": "de"} as CalendarTable,
        {"id": "3", "date": today, "blessing": "3", "language": "en"} as CalendarTable,
      ]
    )
  });
  it("should create an instance of Calendar", () => {
    const calendarInstance = new CalendarController(database);
    expect(calendarInstance).toBeInstanceOf(CalendarController);
  });
  it("should load the calendar", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    expect(calendarInstance.calendar).toHaveLength(3);
    expect(calendarInstance.calendar[0]).toBeInstanceOf(CalendarEntry);
  });
  it("should return a list of calendar entries", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    const list = calendarInstance.calendar;
    expect(list).toHaveLength(3);
    expect(list[0]).toBeInstanceOf(CalendarEntry);
  });
  it("should return a list of calendar entries by year", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    const list = calendarInstance.getCalendarByYear(2021);
    expect(list).toHaveLength(2);
    expect(list[0]).toBeInstanceOf(CalendarEntry);
  });
  it("should return a list of calendar entries by language", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    const list = calendarInstance.getCalendarByLanguage("de");
    expect(list).toHaveLength(2);
    expect(list[0]).toBeInstanceOf(CalendarEntry);
  });
  it("should return a list of calendar entries by language and year", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    const list = calendarInstance.getCalendarByLanguageAndYear("de", 2021);
    expect(list).toHaveLength(2);
    expect(list[0]).toBeInstanceOf(CalendarEntry);
  });
  it("should throw an error if no calendar entries are found by language and year", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    expect(() => {
      calendarInstance.getCalendarByLanguageAndYear("en", 2021);
    }).toThrow("No entries found for language en and year 2021");
  });
  it("should return a list of blessing ids by date", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    const list = calendarInstance.getBlessingIdsByDate(literal_date_1);
    expect(list).toEqual(["1"]);
  });
  it("should return a blessing id by date and language", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    const id = calendarInstance.getBlessingIdByDateAndLanguage(literal_date_1, "de");
    expect(id).toEqual("1");
  });
  it("should throw an error if no blessing is found by date and language", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    expect(() => {
      calendarInstance.getBlessingIdByDateAndLanguage(literal_date_1, "en");
    }).toThrow("No entry found for date 2021-02-01 and language en");
  });
  it("should return a blessing id for today by language", async () => {
    const calendarInstance = new CalendarController(database);
    await calendarInstance.load();
    const id = calendarInstance.getBlessingIdForTodayByLanguage("en");
    expect(id).toEqual("3");
  });
  
});