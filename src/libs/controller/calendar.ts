import { CalendarEntry } from "../models/calendarEntry";
import { CalendarControllerInterface } from "./calendar.interface";
import { CalendarTable } from "./database";
import { DatabaseInterface } from "./database.interface";

export class CalendarController implements CalendarControllerInterface{
  private _database: DatabaseInterface;
  private _calendar: CalendarEntry[];
  constructor(database: DatabaseInterface) {
    this._database = database;
    this._calendar = [];
  }
  async load() {
    const result = await this._database.query<CalendarTable>("SELECT id, date, blessing, language FROM calendar");
    for (const entry of result) {
      this._calendar.push(
        new CalendarEntry(
          entry.id, 
          entry.date.toISOString().split('T')[0], 
          entry.blessing, 
          entry.language
          )
        );
    }
  }
  get calendar() {
    return this._calendar;
  }
  getBlessingIdByDateAndLanguage(date: string, language: string) {
    const entry = this._calendar.find((entry) => entry.date === date && entry.languageKey === language);
    if (!entry) {
      throw new Error(`No entry found for date ${date} and language ${language}`);
    } else {
      return entry.blessingKey;
    }
  }
  getBlessingIdForTodayByLanguage(language: string) {
    const today = new Date().toISOString().split('T')[0];
    return this.getBlessingIdByDateAndLanguage(today, language);
  }
  getCalendarByYear(year: number) {
    return this._calendar.filter((entry) => entry.date.startsWith(year.toString()));
  }
  getCalendarByLanguage(language: string) {
    return this._calendar.filter((entry) => entry.languageKey === language);
  }
  getCalendarByLanguageAndYear(language: string, year: number) {
    const entries = this._calendar.filter((entry) => entry.languageKey === language && entry.date.startsWith(year.toString()));
    if (entries.length === 0) {
      throw new Error(`No entries found for language ${language} and year ${year}`);
    } else {
      return entries;
    }
  }

  getBlessingIdsByDate(date: string) {
    return this._calendar.filter((entry) => entry.date === date).map((entry) => entry.blessingKey);
  }
}