import { RowDataPacket } from "mysql2";

export interface DatabaseInterface {
  query<T extends RowDataPacket>(sql: string): Promise<T[]>;
  isReady(): Promise<boolean>;
}