import { DatabaseSettings } from './database.settings';

const host = 'localhost'
const user = 'root'
const password = 'password'
const database = 'test'
const port = 3306
const connectionLimit = 10

const errorMessage = 'All Database settings are required'

describe('Controller :: DatabaseSettings', () => {
  it('should create an instance of DatabaseSettings', () => {
    const settings = new DatabaseSettings(host, user, password, database, port, connectionLimit);
    expextToBeInstaceOfDatabaseSettings(settings);
  });
  it('should throw an error if no host is provided', () => {
    expect(() => {
      new DatabaseSettings('', user, password, database, port, connectionLimit);
    }).toThrow(errorMessage);
  });
  it('should throw an error if no user is provided', () => {
    expect(() => {
      new DatabaseSettings(host, '', password, database, port, connectionLimit);
    }).toThrow(errorMessage);
  });
  it('should throw an error if no password is provided', () => {
    expect(() => {
      new DatabaseSettings(host, user, '', database, port, connectionLimit);
    }).toThrow(errorMessage);
  });
  it('should throw an error if no database is provided', () => {
    expect(() => {
      new DatabaseSettings(host, user, password, '', port, connectionLimit);
    }).toThrow(errorMessage);
  });
  it('should throw an error if no port is provided', () => {
    expect(() => {
      new DatabaseSettings(host, user, password, database, 0, connectionLimit);
    }).toThrow(errorMessage);
  });
  it('should throw an error if no connectionLimit is provided', () => {
    expect(() => {
      new DatabaseSettings(host, user, password, database, port, 0);
    }).toThrow(errorMessage);
  });
  it('should return the host', () => {
    const settings = new DatabaseSettings(host, user, password, database, port, connectionLimit);
    expect(settings.host).toBe(host);
  });
  it('should return the user', () => {
    const settings = new DatabaseSettings(host, user, password, database, port, connectionLimit);
    expect(settings.user).toBe(user);
  });
  it('should return the password', () => {
    const settings = new DatabaseSettings(host, user, password, database, port, connectionLimit);
    expect(settings.password).toBe(password);
  });
  it('should return the database', () => {
    const settings = new DatabaseSettings(host, user, password, database, port, connectionLimit);
    expect(settings.database).toBe(database);
  });
  it('should return the port', () => {
    const settings = new DatabaseSettings(host, user, password, database, port, connectionLimit);
    expect(settings.port).toBe(port);
  });
  it('should return the connection limit', () => {
    const settings = new DatabaseSettings(host, user, password, database, port, connectionLimit);
    expect(settings.connectionLimit).toBe(connectionLimit);
  });
});
function expextToBeInstaceOfDatabaseSettings(settings: DatabaseSettings): void {
  expect(settings).toBeInstanceOf(DatabaseSettings);
  expect((settings as DatabaseSettings).host).toBe(host);
  expect((settings as DatabaseSettings).user).toBe(user);
  expect((settings as DatabaseSettings).password).toBe(password);
  expect((settings as DatabaseSettings).database).toBe(database);
  expect((settings as DatabaseSettings).port).toBe(port);
}