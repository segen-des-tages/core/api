import { DatabaseSettingsInterface } from "./database.settings.interface";

export class DatabaseSettings implements DatabaseSettingsInterface{
  private _host: string;
  private _user: string;
  private _password: string;
  private _database: string;
  private _port: number;
  private _connectionLimit: number;
  constructor(host: string, user: string, password: string, database: string, port: number, connectionLimit: number) {
    if (!host || !user || !password || !database || !port || !connectionLimit) {
      throw new Error("All Database settings are required");
    }
    this._host = host;
    this._user = user;
    this._password = password;
    this._database = database;
    this._port = port;
    this._connectionLimit = connectionLimit;
  }
  public get host(): string {
    return this._host;
  }
  public get user(): string {
    return this._user;
  }
  public get password(): string {
    return this._password;
  }
  public get database(): string {
    return this._database;
  }
  public get port(): number {
    return this._port;
  }
  public get connectionLimit(): number {
    return this._connectionLimit;
  }
}