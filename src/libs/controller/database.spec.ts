import { Database } from './database';
import { MockDatabaseSettings } from './__mocks__/database.settings';
import { mockRelease, mockEnd, mockQuery, mockPool } from "./__mocks__/mysql2/promise";
// Make sure to mock the correct path if you're using mysql2/promise
jest.mock("mysql2/promise");

describe('Controller :: Database', () => {
  let db: Database;
  const settings = new MockDatabaseSettings(
    "localhost",
    "user",
    "password",
    "database",
    3306,
    10
  );
  const mockSQL = "SELECT * FROM table";

  beforeEach(() => {
    jest.clearAllMocks(); // Clear mocks between tests
    db = new Database(settings);
  });

  it("should not work when using empty settings", () => {
    expect(() => {
      new Database(undefined as unknown as MockDatabaseSettings);
    }).toThrow("Database settings are required");
  });
  it("should not work when using empty host", () => {
    expect(() => {
      new Database(new MockDatabaseSettings("", "user", "password", "database", 3306, 10));
    }).toThrow("Database host is required");
  });
  it("should not work when using empty user", () => {
    expect(() => {
      new Database(new MockDatabaseSettings("localhost", "", "password", "database", 3306, 10));
    }).toThrow("Database user is required");
  });
  it("should not work when using empty password", () => {
    expect(() => {
      new Database(new MockDatabaseSettings("localhost", "user", "", "database", 3306, 10));
    }).toThrow("Database password is required");
  });
  it("should not work when using empty database", () => {
    expect(() => {
      new Database(new MockDatabaseSettings("localhost", "user", "password", "", 3306, 10));
    }).toThrow("Database name is required");
  });
  it("should not work when using empty port", () => {
    expect(() => {
      new Database(new MockDatabaseSettings("localhost", "user", "password", "database", 0, 10));
    }).toThrow("Database port is required");
  });
  it("should not work when using empty connection limit", () => {
    expect(() => {
      new Database(new MockDatabaseSettings("localhost", "user", "password", "database", 3306, 0));
    }).toThrow("Database connection limit is required");
  });
  it('should connect and query the database', async () => {
    const result = await db.query(mockSQL);

    // Assuming the first element of the resolved value array is the query result
    expect(result).toEqual([{ id: 1 }]);
    expect(mockQuery).toHaveBeenCalledWith(mockSQL);
  });

  it('should release the connection back to the pool after querying', async () => {
    await db.query(mockSQL);

    expect(mockRelease).toHaveBeenCalled();
  });

  it('should end the pool when disconnecting', async () => {
    await db.disconnect();
    expect(mockEnd).toHaveBeenCalled();
  });

  it('should throw an error if the query fails', async () => {
    mockQuery.mockRejectedValueOnce(new Error("Query failed"));

    await expect(db.query(mockSQL)).rejects.toThrow("Query failed");
  });
  
  it('should check if the pool is ready', async () => {
    const ready = await db.isReady();
    expect(ready).toBe(true);
  });

  it('should return false if the pool is not ready', async () => {
    mockPool.getConnection.mockRejectedValueOnce(new Error("Pool is not ready"));

    const ready = await db.isReady();
    expect(ready).toBe(false);
  });
});
