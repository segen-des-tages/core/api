import { Pool, RowDataPacket, createPool } from "mysql2/promise";
import { DatabaseInterface } from "./database.interface";
import { DatabaseSettingsInterface } from "./database.settings.interface";

export interface LanguagesTable extends RowDataPacket {
  key: string;
  name: string;
  active: boolean;
}

export interface BlessingsTable extends RowDataPacket {
  id: string;
  language: string;
  blessing: string;
}

export interface CalendarTable extends RowDataPacket {
  id: string;
  date: Date;
  blessing: string;
  language: string;
}

export class Database implements DatabaseInterface{
  private _pool: Pool;
  private _settings: DatabaseSettingsInterface;
  constructor(settings: DatabaseSettingsInterface) {
    if(!settings) {
      throw new Error("Database settings are required");
    }
    this._settings = settings;
    this._pool = createPool({
      host: this._settings.host,
      user: this._settings.user,
      password: this._settings.password,
      database: this._settings.database,
      port: this._settings.port,
      connectionLimit: this._settings.connectionLimit
    });
  }

  public async disconnect(): Promise<void> {
    await this._pool.end();
  }
  public async query<T extends RowDataPacket>(sql: string): Promise<T[]> {
    let connection;
    try {
      connection = await this._pool.getConnection();
      const [results] = await connection.query<T[]>(sql);
      return results;
    } catch (error: unknown) {
      if (error instanceof AggregateError) {
        throw new Error(`Database Error: ${error.errors[0].code}`);
      } else if (error instanceof Error) {
        throw new Error(`Database Error: ${error.message}`);
      } else {
        throw new Error('Unknown error');
      }
    } finally {
      if (connection) {
        connection.release();
      }
    }
  }
  public async isReady(): Promise<boolean> {
    // Try to get a connection from the pool as a readiness check
    try {
      const connection = await this._pool.getConnection();
      connection.release(); // Immediately release the connection
      return true; // If a connection was successfully obtained, the pool is ready
    } catch (error) {
      return false; // If an error occurs, the pool is not ready
    }
  }
}