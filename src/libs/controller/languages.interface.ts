import { Language } from "../models/language";

export interface LanguagesControllerInterface {
  load(): Promise<void>;
  languages: Language[];
  getLanguage(key: string): Language;
  getActiveLanguages(): Language[];
}