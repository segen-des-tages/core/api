import { LanguageController } from './languages';
import { MockDatabase } from './__mocks__/database';
import { Language } from '../models/language';
import { LanguagesTable } from './database';

const database = new MockDatabase();

describe('Controller :: Languages', () => {
  beforeAll(() => {
    database.setQueryResult(
      "SELECT `key`, name, active FROM languages",
      [
        {"key": "en", "name": "English", "active": true} as LanguagesTable,
        {"key": "es", "name": "Spanish", "active": true} as LanguagesTable,
        {"key": "fr", "name": "French", "active": false} as LanguagesTable,
      ]
    )
  });
  it("should create an instance of Languages", () => {
    const languages = new LanguageController(database);
    expect(languages).toBeInstanceOf(LanguageController);
  });
  it("should load the languages", async () => {
    const languages = new LanguageController(database);
    await languages.load();
    expect(languages.languages).toHaveLength(3);
    expect(languages.languages[0]).toBeInstanceOf(Language);
  });
  it("should throw an error if load fails", async () => {
    database._throwError = true;
    const languages = new LanguageController(database);
    await expect(languages.load()).rejects.toThrow("Database Error: MockDatabase Error");
    database._throwError = false;
  });
  it("should return a list of languages", async () => {
    database._throwError = false;
    const languages = new LanguageController(database);
    await languages.load();
    const list = languages.languages;
    expect(list).toHaveLength(3);
    expect(list[0]).toBeInstanceOf(Language);
  });
  it("should return a language by key", async () => {
    const languages = new LanguageController(database);
    await languages.load();
    const language = languages.getLanguage("en");
    expect(language).toBeInstanceOf(Language);
    expect(language?.key).toBe("en");
  });
  it("should throw an error if language not found", async () => {
    const languages = new LanguageController(database);
    await languages.load();
    expect(() => {
      languages.getLanguage("xx");
    }).toThrow("Language not found: xx");
  });
  it("should return a list of active languages", async () => {
    const languages = new LanguageController(database);
    await languages.load();
    const list = languages.getActiveLanguages();
    expect(list).toHaveLength(2);
    expect(list[0]).toBeInstanceOf(Language);
  });
});