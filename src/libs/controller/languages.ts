import { Language } from "../models/language";
import { LanguagesTable } from "./database";
import { DatabaseInterface } from "./database.interface";
import { LanguagesControllerInterface } from "./languages.interface";

export class LanguageController implements LanguagesControllerInterface{
  private _database: DatabaseInterface;
  private _languages: Language[];
  constructor(database: DatabaseInterface) {
    this._database = database;
    this._languages = [];
  }
  public async load(): Promise<void> {
    try {
      const results = await this._database.query<LanguagesTable>('SELECT `key`, name, active FROM languages');
      this._languages = results.map((result) => {
        return new Language(result.key, result.name, result.active);
      });
    } catch (error: unknown) {
      if (error instanceof Error) {
        throw new Error(`Database Error: ${error.message}`);
      } else {
        throw new Error('Unknown error');
      }
    }
  }
  public get languages(): Language[] {
    return this._languages;
  }
  public getLanguage(key: string): Language { 
    const found = this._languages.find((language) => language.key === key);
    if (!found) {
      throw new Error(`Language not found: ${key}`);
    } else {
      return found;
    }
  }
  public getActiveLanguages(): Language[] {
    return this._languages.filter((language) => language.isActive);
  }
}