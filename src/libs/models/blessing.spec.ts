import { Language } from './language';
import { Blessing } from './blessing';

const id = '1';
const language = new Language('en', 'English', true);
const fakeLanguage = new Language('en', 'FAKE', false);
const languageKey = 'en';
const blessing = 'May the force be with you';

describe('Model :: Blessing', () => {
  it('should create an instance of Blessing', () => {
    const blessingInstance = new Blessing(id, blessing, languageKey);

    expect(blessingInstance).toBeInstanceOf(Blessing);
    expect((blessingInstance as Blessing).id).toBe(id);
    expect((blessingInstance as Blessing).languageKey).toBe(languageKey);
    expect((blessingInstance as Blessing).blessing).toBe(blessing);
  });
  it('shpuld throw an error if id is empty', () => {
    expect(() => {
      new Blessing('', blessing, languageKey);
    }).toThrow('Id is required');
  });
  it('shpuld throw an error if blessing is empty', () => {
    expect(() => {
      new Blessing('1', '', languageKey);
    }).toThrow('Blessing is required');
  });
  it('should return the id', () => {
    const blessingInstance = new Blessing(id, blessing, languageKey);
    expect(blessingInstance.id).toBe(id);
  });
  it('should return the blessing', () => {
    const blessingInstance = new Blessing(id, blessing, languageKey);
    expect(blessingInstance.blessing).toBe(blessing);
  });
  it('should return the language', () => {
    const blessingInstance = new Blessing(id, blessing, languageKey);
    expect(blessingInstance.languageKey).toBe(languageKey);
  });
  it('should return the language after setting it', () => {
    const blessingInstance = new Blessing(id, blessing, languageKey);
    blessingInstance.language = language;
    expect(blessingInstance.language.key).toBe(language.key);
    expect(blessingInstance.language.isActive).toBe(language.isActive);
  });
  it('should return the default fake language if not setting it', () => {
    const blessingInstance = new Blessing(id, blessing, languageKey);
    expect(blessingInstance.language.key).toBe(fakeLanguage.key);
    expect(blessingInstance.language.isActive).toBe(fakeLanguage.isActive);
  });
});
