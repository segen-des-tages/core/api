import { Language } from "./language";

export class Blessing {
  private _id: string ;
  private _blessing: string;
  private _languageKey: string;
  private _language: Language;
  constructor(id: string, blessing: string, languageKey: string) {
    if (!id) {
      throw new Error('Id is required');
    } else {
      this._id = id;
    }
    if (!blessing) {
      throw new Error('Blessing is required');
    } else {
      this._blessing = blessing;
    }
    this._languageKey = languageKey;
    this._language = new Language(languageKey, 'FAKE', false);
  }
  public get id(): string {
    return this._id;
  }
  public get blessing(): string {
    return this._blessing;
  }
  public get languageKey(): string {
    return this._languageKey;
  }
  public get language(): Language {
    return this._language;
  }
  public set language(language: Language) {
    this._language = language;
  }
  public toJSON(): unknown {
    return {
      id: this._id,
      blessing: this._blessing,
      language: this._languageKey
    };
  }
}