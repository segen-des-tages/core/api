import { CalendarEntry } from './calendarEntry';

const id = '1';
const date = '2021-01-01';
const languageKey = 'en';
const blessingKey = '1';

describe('Model :: CalendarEntry', () => {
  it('should create an instance of CalendarEntry', () => {
    const calendarEntryInstance = new CalendarEntry(id, date, blessingKey, languageKey);

    expect(calendarEntryInstance).toBeInstanceOf(CalendarEntry);
    expect((calendarEntryInstance as CalendarEntry).date).toBe(date);
    expect((calendarEntryInstance as CalendarEntry).blessingKey).toBe(blessingKey);
    expect((calendarEntryInstance as CalendarEntry).languageKey).toBe(languageKey);
  });
  it('should throw an error if id is empty', () => {
    expect(() => {
      new CalendarEntry('', date, blessingKey, languageKey);
    }).toThrow('Id is required');
  });
  it('should throw an error if date is empty', () => {
    expect(() => {
      new CalendarEntry(id, '', blessingKey, languageKey);
    }).toThrow('Date is required');
  });
  it('should return the date', () => {
    const calendarEntry = new CalendarEntry(id, date, blessingKey, languageKey);
    expect(calendarEntry.date).toBe(date);
  });
  it('should return the language key', () => {
    const calendarEntry = new CalendarEntry(id, date, blessingKey, languageKey);
    expect(calendarEntry.languageKey).toBe(languageKey);
  });
  it('should return the blessing key', () => {
    const calendarEntry = new CalendarEntry(id, date, blessingKey, languageKey);
    expect(calendarEntry.blessingKey).toBe(blessingKey);
  });
  it('should return a JSON representation', () => {
    const calendarEntry = new CalendarEntry(id, date, blessingKey, languageKey);
    expect(calendarEntry.toJSON()).toEqual({
      id: id,
      date: date,
      blessingKey: blessingKey,
      languageKey: languageKey,
    });
  });
});
