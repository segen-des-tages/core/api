export class CalendarEntry {
  private _id: string;
  private _date: string;
  private _blessingKey: string;
  private _languageKey: string;
  constructor(id: string, date: string, blessingKey: string, languageKey: string) {
    if (!id) {
      throw new Error('Id is required');
    } else {
      this._id = id;
    }
    if (!date) {
      throw new Error('Date is required');
    } else {
      this._date = date;
    }
    this._blessingKey = blessingKey;
    this._languageKey = languageKey;
  }
  public get date() {
    return this._date;
  }
  public get languageKey() {
    return this._languageKey;
  }
  public get blessingKey() {
    return this._blessingKey;
  }
  public toJSON() {
    return {
      id: this._id,
      date: this._date,
      blessingKey: this._blessingKey,
      languageKey: this._languageKey
    };
  }
}