import { Language } from './language';

describe('Model :: Language', () => {
  it('should create an instance of Language', () => {
    const key = 'en';
    const name = 'English';
    const active = true;
    const langInstance = new Language(key, name, active);

    expect(langInstance).toBeInstanceOf(Language);
    expect((langInstance as Language).key).toBe(key);
    expect((langInstance as Language).name).toBe(name);
    expect((langInstance as Language).isActive).toBe(active);

    expect(langInstance.key).toBe(key);
    expect(langInstance.isActive).toBe(active);
  });
  it('shpuld throw an error if key is empty', () => {
    expect(() => {
      new Language('', 'English', true);
    }).toThrow('Key is required');
  });
  it('shpuld throw an error if name is empty', () => {
    expect(() => {
      new Language('en', '', true);
    }).toThrow('Name is required');
  });
});
