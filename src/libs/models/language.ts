export class Language {
  private _key: string;
  private _name: string;
  private _active: boolean;
  constructor(key: string, name: string, active: boolean) {
    if (!key) {
      throw new Error('Key is required');
    } else {
      this._key = key;
    }
    if (!name) {
      throw new Error('Name is required');
    } else {
      this._name = name;
    }
    this._active = active;
  }
  public get key(): string {
    return this._key;
  }
  public get name(): string {
    return this._name;
  }
  public get isActive(): boolean {
    return this._active;
  }
  public toJSON(): unknown {
    return {
      key: this._key,
      name: this._name,
      active: this._active
    }
  }
}