import request from 'supertest';
import express, { Express } from 'express';
import { BlessingsRoutes } from './blessings';
import { Blessing } from '../models/blessing';
import { MockBlessingsController } from '../controller/__mocks__/blessings';

const blessingsController = new MockBlessingsController();

const blessingA = 'A Blessing';
const blessingB = 'Another Blessing';

describe('Routes :: Blessings', () => {
  let app: Express;

  beforeAll(() => {
    app = express();
    app.use(express.json());

    // Initialize LanguageController and LanguageRoutes
    const blessingRoutes = new BlessingsRoutes(blessingsController);
    blessingsController._blessings = [
      new Blessing('1', blessingA, 'en'),
      new Blessing('2', blessingB, 'de'),
    ]
    app.use('/blessings', blessingRoutes.router);
  });

  it('GET /blessings/ should return all blessings', async () => {
    const response = await request(app).get('/blessings');
    expect(response.status).toBe(200);
    expect(response.body).toEqual([
      { id: '1', blessing: blessingA, language: 'en' },
      { id: '2', blessing: blessingB, language: 'de' },
    ]);
  });

  it('GET /blessings/:id should return a single blessing', async () => {
    const response = await request(app).get('/blessings/1');
    expect(response.status).toBe(200);
    expect(response.body).toEqual({ id: '1', blessing: blessingA, language: 'en' });
  });

  it('GET /blessings/:id should return 404 if the blessing is not found', async () => {
    blessingsController._provokeError = true;
    const response = await request(app).get('/blessings/xx');
    expect(response.status).toBe(404);
    expect(response.body).toEqual({ message: 'Blessing not found: xx' });
    blessingsController._provokeError = false;
  });


  it('GET /blessings/language/:language should return all blessings for a language', async () => {
    const response = await request(app).get('/blessings/language/de');
    expect(response.status).toBe(200);
    expect(response.body).toEqual([  { id: '2', blessing: blessingB, language: 'de' }]);
  });

});