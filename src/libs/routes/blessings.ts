import { Router, Request, Response } from 'express';
import { BlessingsControllerInterface } from '../controller/blessings.interface';

export class BlessingsRoutes {
  public router: Router;
  private blessingsController: BlessingsControllerInterface;

  constructor(blessingsController: BlessingsControllerInterface) {
    this.router = Router();
    this.blessingsController = blessingsController;
    this.initRoutes();
  }

  private initRoutes(): void {
    this.router.get('', this.getAllBlessings.bind(this));   
    this.router.get('/language/:language', this.getBlessingsByLanguage.bind(this));
    this.router.get('/:id', this.getBlessingsByID.bind(this));

  }

  private async getAllBlessings(req: Request, res: Response): Promise<void> {
    const blessings = this.blessingsController.blessings;
    const json = []
    for (const blessing of blessings) {
      json.push(blessing.toJSON());
    }
    res.json(json);
  }
  private async getBlessingsByLanguage(req: Request, res: Response): Promise<void> {
    const blessings = this.blessingsController.getBlessingsByLanguage(req.params.language);
    const json = []
    for (const blessing of blessings) {
      json.push(blessing.toJSON());
    }
    res.json(json);
  }
  private getBlessingsByID(req: Request, res: Response): void {
    try {
      const blessing = this.blessingsController.getBlessing(req.params.id);
      res.json(blessing.toJSON());
    } catch (error: unknown) {
      if (error instanceof Error) {
        res.status(404).json({ message: error.message });
      } else {
        res.status(500).json({ message: 'Internal Server Error' });
      }
    }
  }
}
/**
   * A Blessing
   * @typedef {object} Blessing
   * @property {string} id - The id of the blessing
   * @property {string} language - The language of the blessing
   * @property {string} blessing - The text of the blessing
   */
/**
 * GET /blessings
 * @summary Returns all blessings available
 * @tags blessings
 * @return {array<Language>} 200 - An array of blessings
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error 
 */
/**
 * GET /blessings/language/{language}
 * @summary Returns all blessings available in a language
 * @tags blessings
 * @param {string} language.path.required - The language of the blessings
 * @return {array<Language>} 200 - An array of blessings
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error 
 */
/**
   * GET /blessing/{id}
   * @summary Returns a blessing by id
   * @tags blessings
   * @param {string} id.path.required - The id of the blessing
   * @return {Blessing} 200 - The blessing object
   * @return {string} 404 - Not Found
   * @return {string} 503 - Service Unavailable
   */