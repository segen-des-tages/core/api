import request from 'supertest';
import express, { Express } from 'express';
import { CalendarRoutes } from './calendar';
import { MockCalendarController } from "../controller/__mocks__/calendar";
import { MockBlessingsController } from "../controller/__mocks__/blessings";
import { Blessing } from '../models/blessing';
import { CalendarEntry } from '../models/calendarEntry';

const calendarController = new MockCalendarController();
const blessingsController = new MockBlessingsController();

const blessing_1 = new Blessing('1', 'A Blessing', 'de');
const blessing_2 = new Blessing('2', 'Another Blessing', 'en');

const calendarEntry_1 = new CalendarEntry('1', '2021-01-01', '1', 'de');
const calendarEntry_2 = new CalendarEntry('2', '2021-01-02', '2', 'en');
const calendarEntry_3 = new CalendarEntry('3', '2021-01-03', '1', 'de');
const calendarEntry_4 = new CalendarEntry('4', '2022-01-04', '2', 'en');

describe("Routes :: Calendar", () => {
  let app: Express;

  beforeAll(() => {
    app = express();
    app.use(express.json());

    // Initialize LanguageController and LanguageRoutes
    const calendarRoutes = new CalendarRoutes(calendarController, blessingsController);
    blessingsController._blessings = [
      blessing_1,
      blessing_2
    ]
    calendarController._calendar = [
      calendarEntry_1,
      calendarEntry_2,
      calendarEntry_3,
      calendarEntry_4
    ]
    app.use('/calendar', calendarRoutes.router);
  });

    it("GET /calendar returns all calendar entries", async () => {
      const response = await request(app).get('/calendar');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual([
        calendarEntry_1.toJSON(),
        calendarEntry_2.toJSON(),
        calendarEntry_3.toJSON(),
        calendarEntry_4.toJSON()
      ]);
    });

    it("GET /today/:language returns the blessing for today in a language", async () => {
      const response = await request(app).get('/calendar/today/de');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual(blessing_1.toJSON());
    });

    it("GET /today/:language returns 404 if no blessing is found", async () => {
      const response = await request(app).get('/calendar/today/fr');
      expect(response.status).toEqual(404);
      expect(response.body).toEqual({ message: "No entry found for date " + calendarEntry_1.date + " and language fr" });
    });

    it("GET /date/:date returns the blessings for a date", async () => {
      const response = await request(app).get('/calendar/date/2021-01-01');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual([
        blessing_1.toJSON()
      ]);
    });

    it("GET /date/:date/:language returns the blessing for a date in a language", async () => {
      const response = await request(app).get('/calendar/date/2021-01-01/de');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual(blessing_1.toJSON());
    });

    it("GET /date/:date/:language returns 404 if no blessing is found", async () => {
      const response = await request(app).get('/calendar/date/2021-01-01/fr');
      expect(response.status).toEqual(404);
      expect(response.body).toEqual({ message: "No entry found for date 2021-01-01 and language fr" });
    });

    it("GET /year/:year returns the calendar entries for a year", async () => {
      const response = await request(app).get('/calendar/year/2021');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual([
        calendarEntry_1.toJSON(),
        calendarEntry_2.toJSON(),
        calendarEntry_3.toJSON()
      ]);
    });

    it("GET /language/:language returns the calendar entries for a language", async () => {
      const response = await request(app).get('/calendar/language/de');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual([
        calendarEntry_1.toJSON(),
        calendarEntry_3.toJSON()
      ]);
    });

    it("GET /:language/:year returns the calendar entries for a language and year", async () => {
      const response = await request(app).get('/calendar/de/2021');
      expect(response.status).toEqual(200);
      expect(response.body).toEqual([
        calendarEntry_1.toJSON(),
        calendarEntry_3.toJSON()
      ]);
    });

});