import { Router, Request, Response } from 'express';
import { CalendarControllerInterface } from '../controller/calendar.interface';
import { BlessingsControllerInterface } from '../controller/blessings.interface';

export class CalendarRoutes {
  public router: Router;
  private calendarController: CalendarControllerInterface;
  private blessingsController: BlessingsControllerInterface;

  constructor(
    calendarController: CalendarControllerInterface,
    blessingsController: BlessingsControllerInterface) {
    this.router = Router();
    this.calendarController = calendarController;
    this.blessingsController = blessingsController;
    this.initRoutes();
  }

  private initRoutes(): void {
    this.router.get('', this.getAllCalendarEntries.bind(this));   
    this.router.get('/today/:language', this.getBlessingForTodayByLanguage.bind(this));
    this.router.get('/date/:date/', this.getBlessingsForDate.bind(this));
    this.router.get('/date/:date/:language', this.getBlessingForDateAndLanguage.bind(this));
    this.router.get('/year/:year', this.getCalendarForYear.bind(this));
    this.router.get('/language/:language', this.getCalendarForLanguage.bind(this));
    this.router.get('/:language/:year', this.getCalendarForLanguageAndYear.bind(this));
  }

  private async getAllCalendarEntries(req: Request, res: Response): Promise<void> {
    const calendar = this.calendarController.calendar;
    const json = []
    for (const entry of calendar) {
      json.push(entry.toJSON());
    }
    res.json(json);
  }
  private async getBlessingForTodayByLanguage(req: Request, res: Response): Promise<void> {
    try {
      const blessingId = this.calendarController.getBlessingIdForTodayByLanguage(req.params.language);
      const blessing = this.blessingsController.getBlessing(blessingId);
      res.json(blessing.toJSON());
    } catch (error: unknown) {
      if (error instanceof Error) {
        res.status(404).json({ message: error.message });
      } else {
        res.status(500).json({ message: 'Internal Server Error' });
      }
    }
  }
  private async getBlessingsForDate(req: Request, res: Response): Promise<void> {
    const blessings = this.calendarController.getBlessingIdsByDate(req.params.date);
    const json = []
    for (const blessingId of blessings) {
      const blessing = this.blessingsController.getBlessing(blessingId);
      json.push(blessing.toJSON());
    }
    res.json(json);
  }
  private async getBlessingForDateAndLanguage(req: Request, res: Response): Promise<void> {
    try {
      const blessingId = this.calendarController.getBlessingIdByDateAndLanguage(req.params.date, req.params.language);
      const blessing = this.blessingsController.getBlessing(blessingId);
      res.json(blessing.toJSON());
    } catch (error: unknown) {
      if (error instanceof Error) {
        res.status(404).json({ message: error.message });
      } else {
        res.status(500).json({ message: 'Internal Server Error' });
      }
    }
  }
  private async getCalendarForYear(req: Request, res: Response): Promise<void> {
    const calendar = this.calendarController.getCalendarByYear(parseInt(req.params.year));
    const json = []
    for (const entry of calendar) {
      json.push(entry.toJSON());
    }
    res.json(json);
  }
  private async getCalendarForLanguage(req: Request, res: Response): Promise<void> {
    const calendar = this.calendarController.getCalendarByLanguage(req.params.language);
    const json = []
    for (const entry of calendar) {
      json.push(entry.toJSON());
    }
    res.json(json);
  }
  private async getCalendarForLanguageAndYear(req: Request, res: Response): Promise<void> {
    const calendar = this.calendarController.getCalendarByLanguageAndYear(req.params.language, parseInt(req.params.year));
    const json = []
    for (const entry of calendar) {
      json.push(entry.toJSON());
    }
    res.json(json);
  }
}
  /**
   * A Calendar Entry
   * @typedef {object} CalendarEntry
   * @property {string} id - The id of the calendar entry
   * @property {string} date - The date of the calendar entry
   * @property {string} language - The language of the calendar entry
   * @property {string} blessing - The blessing of the calendar entry
   */
/**
 * GET /calendar
 * @summary Returns all calendar entries available
 * @tags calendar
 * @return {array<CalendarEntry>} 200 - An array of calendar entries
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error
 */
/**
 * GET /calendar/today/{language}
 * @summary Returns the blessing for today in a language
 * @tags calendar
 * @param {string} language.path.required - The language of the blessing
 * @return {Blessing} 200 - The blessing object
 * @return {string} 404 - Not Found
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error
 */
/**
 * GET /calendar/date/{date}
 * @summary Returns all blessings for a date
 * @tags calendar
 * @param {string} date.path.required - The date of the blessings
 * @return {array<Blessing>} 200 - An array of blessings
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error
 * @return {string} 404 - Not Found
 * @return {string} 500 - Internal Server Error
 */
/**
 * GET /calendar/date/{date}/{language}
 * @summary Returns the blessing for a date in a language
 * @tags calendar
 * @param {string} date.path.required - The date of the blessings
 * @param {string} language.path.required - The language of the blessings
 * @return {Blessing} 200 - The blessing object
 * @return {string} 404 - Not Found
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error
 */
/**
 * GET /calendar/year/{year}
 * @summary Returns all calendar entries for a year
 * @tags calendar
 * @param {string} year.path.required - The year of the calendar entries
 * @return {array<CalendarEntry>} 200 - An array of calendar entries
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error
 * @return {string} 404 - Not Found
 */
/**
 * GET /calendar/language/{language}
 * @summary Returns all calendar entries for a language
 * @tags calendar
 * @param {string} language.path.required - The language of the calendar entries
 * @return {array<CalendarEntry>} 200 - An array of calendar entries
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error
 * @return {string} 404 - Not Found
 */
/**
 * GET /calendar/{language}/{year}
 * @summary Returns all calendar entries for a language and year
 * @tags calendar
 * @param {string} language.path.required - The language of the calendar entries
 * @param {string} year.path.required - The year of the calendar entries
 * @return {array<CalendarEntry>} 200 - An array of calendar entries
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error
 * @return {string} 404 - Not Found
 */
