import request from 'supertest';
import express, { Express } from 'express';
import { LanguageRoutes } from './languages';
import { MockLanuagesController } from '../controller/__mocks__/languages';
import { Language } from '../models/language';

const languageController = new MockLanuagesController();

describe('Routes :: Languages', () => {
  let app: Express;

  beforeAll(() => {
    app = express();
    app.use(express.json());

    // Initialize LanguageController and LanguageRoutes
    const languageRoutes = new LanguageRoutes(languageController);
    languageController._languages = [
      new Language('en', 'English', true),
      new Language('es', 'Spanish', false),
    ]
    app.use('/languages', languageRoutes.router);
  });

  it('GET /languages/ should return all languages', async () => {
    const response = await request(app).get('/languages');
    expect(response.status).toBe(200);
    expect(response.body).toEqual([
      { key: 'en', name: 'English', active: true },
      { key: 'es', name: 'Spanish', active: false },
    ]);
  });

  it('GET /languages/:key should return a single language', async () => {
    const response = await request(app).get('/languages/en');
    expect(response.status).toBe(200);
    expect(response.body).toEqual({ key: 'en', name: 'English', active: true });
  });

  it('GET /languages/:key should return 404 if the language is not found', async () => {
    languageController._provokeError = true;
    const response = await request(app).get('/languages/xx');
    expect(response.status).toBe(404);
    expect(response.body).toEqual({ message: 'Language not found: xx' });
    languageController._provokeError = false;
  });


  it('GET /languages/active should return all active languages', async () => {
    const response = await request(app).get('/languages/active');
    expect(response.status).toBe(200);
    expect(response.body).toEqual([{ key: 'en', name: 'English', active: true }]);
  });

});