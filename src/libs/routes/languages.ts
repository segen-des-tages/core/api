import { Router, Request, Response } from 'express';
import { LanguagesControllerInterface } from '../controller/languages.interface';

export class LanguageRoutes {
  public router: Router;
  private languageController: LanguagesControllerInterface;

  constructor(languageController: LanguagesControllerInterface) {
    this.router = Router();
    this.languageController = languageController;
    this.initRoutes();
  }

  private initRoutes(): void {
    this.router.get('', this.getAllLanguages.bind(this));   
    this.router.get('/active', this.getActiveLanguages.bind(this));
    this.router.get('/:key', this.getLanguageByKey.bind(this));
  }

  private async getAllLanguages(req: Request, res: Response): Promise<void> {
    const languages = this.languageController.languages;
    const json = []
    for (const language of languages) {
      json.push(language.toJSON());
    }
    res.json(json);
  }
  private async getActiveLanguages(req: Request, res: Response): Promise<void> {
    const languages = this.languageController.getActiveLanguages();
    const json = []
    for (const language of languages) {
      json.push(language.toJSON());
    }
    res.json(json);
  }
  private getLanguageByKey(req: Request, res: Response): void {
    try {
      const language = this.languageController.getLanguage(req.params.key);
      res.json(language.toJSON());
    } catch (error: unknown) {
      if (error instanceof Error) {
        res.status(404).json({ message: error.message });
      } else {
        res.status(500).json({ message: 'Internal Server Error' });
      }
    }
  }
}
/**
 * A Language
 * @typedef {object} Language
 * @property {string} name - The name of the language
 * @property {string} key - The code of the language
 * @property {boolean} active - The status of the language
 */
/**
 * GET /languages
 * @summary Returns all languages available
 * @tags languages
 * @return {array<Language>} 200 - An array of languages
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error 
 */
/**
 * GET /languages/active
 * @summary Returns all active languages available
 * @tags languages
 * @return {array<Language>} 200 - An array of languages
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error 
 */
/**
 * GET /languages/{key}
 * @summary Returns the language by key
 * @tags languages
 * @param {string} key.path.required - The key to the language (e.g. en, es, fr, etc.)
 * @return {Language} 200 - A Language
 * @return {string} 404 - Not Found
 * @return {string} 503 - Service Unavailable
 * @return {string} 500 - Internal Server Error 
 */