import { setup as setupDevServer, teardown as teardownDevServer } from "jest-dev-server";
import request from 'supertest';
import { execSync } from 'child_process';
import fs from 'fs';
import { createPool } from "mysql2/promise";
import * as packageInfo from '../package.json';

const liveTest = process.env.LIVE === 'true'; // If false, the tests will run against the local server, if true, the tests will run against the live server
const base = liveTest ? "https://api.segen-des-tages.de" : "http://localhost:3000";

const databaseImage = 'ghcr.io/segen-des-tages/database:latest';

// eslint-disable-next-line  @typescript-eslint/no-explicit-any
let server: any; // this is part of the jest-dev-server setup and is not typesafe for now.
const version = packageInfo.version;

describe("Live Server Test against " + base, () => {
  
  beforeAll(async () => {
    if(!liveTest) {
      execSync('podman login ghcr.io -u segen-des-tages --password-stdin', {input: fs.readFileSync('.ghcr_token')});
      execSync('podman pull ' + databaseImage);
      execSync('podman run --name sdt-database-test -e MYSQL_ROOT_PASSWORD=segen_des_tages -d -p 33060:3306 ' + databaseImage); 
      await checkMySQLReady();
      server = await setupDevServer({
        command: `npm run serve`,
        launchTimeout: 50000,
        port: 3000,
      });
    }
  }, 30000);

  describe("Main Routes", () => {
    it("GET / should return 200 and the title page", async () => {
      const response = await request(base).get('/');
      expect(response.status).toBe(200);
      expect(response.text).toContain('<h2>Todays blessing</h2>');
    });

    it("GET /_health should return 200", async () => {
      const response = await request(base).get('/_health');
      expect(response.status).toBe(200);
    });

    it("GET /_version should return 200 and current version", async () => {
      const response = await request(base).get('/_version');
      expect(response.status).toBe(200);
      expect(response.text).toContain(version);
    });

    it("GET /_metrics should return 200 and the metrics", async () => {
      const response = await request(base).get('/_metrics');
      expect(response.status).toBe(200);
      expect(response.text).toContain('process_cpu_user_seconds_total');
    });

    it("GET /_api should return 200 and the full API documentation", async () => {
      const response = await request(base).get('/_api/');
      expect(response.status).toBe(200);
      expect(response.text).toContain('@segendestages/api');
    });

    it("GET /_ready should return 200 and the service readiness", async () => {
      const response = await request(base).get('/_ready');
      expect(response.status).toBe(200);
      expect(response.text).toContain('OK');
    });

    it("GET /_config should return 200 and the service config", async () => {
      const response = await request(base).get('/_config');
      expect(response.status).toBe(200);
      expect(response.text).toContain('database');
    });

    it("GET /favicon.ico should return 204", async () => {
      const response = await request(base).get('/favicon.ico');
      expect(response.status).toBe(204);
    });
  });

  describe("Blessing Routes", () => {
    it("GET /blessings should return 200 and the blessings", async () => {
      const response = await request(base).get('/blessings');
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
    });

    it("GET /blessings/language/de should return 200 and the blessings in German", async () => {
      const response = await request(base).get('/blessings/language/de');
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
    });

    it("GET /blessings/:id should return 200 and the blessing", async () => {
      const response = await request(base).get('/blessings/1');
      expect(response.status).toBe(200);
      expect(response.body.id).toBe(1);
    });

  });

  describe("Language Routes", () => {
    it("GET /languages should return 200 and the languages", async () => {
      const response = await request(base).get('/languages');
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
    });
    it("GET /languages/active should return 200 and the active languages", async () => {
      const response = await request(base).get('/languages/active');
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
    });
    it("GET /languages/:id should return 200 and the language", async () => {
      const response = await request(base).get('/languages/de');
      expect(response.status).toBe(200);
      expect(response.body.key).toBe("de");
    });
  });

  describe("Calendar Routes", () => {
    it("GET /calendar should return 200 and the calendar", async () => {
      const response = await request(base).get('/calendar');
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
    });
    it("GET /calendar/today/:language should return 200 and the calendar for today", async () => {
      const response = await request(base).get('/calendar/today/de');
      expect(response.status).toBe(200);
      expect(response.body.id).toBeTruthy();
      expect(response.body.language).toBe("de");
      expect(response.body.blessing).toBeTruthy();
    });
    it("GET /calendar/date/:date should return 200 and the calendar for the date", async () => {
      const response = await request(base).get('/calendar/date/2024-01-01');
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
    });
    it("GET /calendar/date/:date/:language should return 200 and the calendar for the date and language", async () => {
      const response = await request(base).get('/calendar/date/2024-01-01/de');
      expect(response.status).toBe(200);
      expect(response.body.id).toBeTruthy();
      expect(response.body.language).toBe("de");
      expect(response.body.blessing).toBeTruthy();
    });
    it("GET /calendar/year/:year should return 200 and the calendar for the year", async () => {
      const response = await request(base).get('/calendar/year/2024');
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
      expect(response.body[0].id).toBeTruthy();
    });
    it("GET /calendar/language/:language should return 200 and the calendar for the language", async () => {
      const response = await request(base).get('/calendar/language/de');
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
      expect(response.body[0].id).toBeTruthy();
      expect(response.body[0].languageKey).toBe("de");
    });
    it("GET /calendar/:language/:year should return 200 and the calendar for the language and year", async () => {
      const response = await request(base).get('/calendar/de/2024');
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
      expect(response.body[0].id).toBeTruthy();
      expect(response.body[0].languageKey).toBe("de");
      expect(response.body[0].date).toContain("2024");
    });
  });

  afterAll(async () => {
    if(!liveTest) {
      await teardownDevServer(server);
      execSync('podman stop sdt-database-test');
      execSync('podman rm sdt-database-test');
    }
  });
});

// Function to check MySQL readiness
async function checkMySQLReady() {
  let connected = false;
  while (!connected) {
    try {
      const pool = await createPool({
        host: 'localhost',
        user: 'segen_des_tages',
        password: 'segen_des_tages',
        database: 'segen_des_tages',
        port: 33060,
        connectionLimit: 10
      });
      await pool.getConnection();
      console.log('Connected to MySQL!');
      connected = true;
      await pool.end();
    } catch (err) {
      console.error('MySQL connection failed, retrying...', err);
      await new Promise(resolve => setTimeout(resolve, 2000)); // wait 2 seconds before retrying
    }
  }
}