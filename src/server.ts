// server.ts
import { Application } from './app';
import { Database } from './libs/controller/database';
import { DatabaseSettings } from './libs/controller/database.settings';
import { createConfig } from '@libs/config'
import { Log }  from '@libs/log';
const config = createConfig(__dirname + '/../')
const logger = new Log(config.log)

async function main() {
  try {
    logger.info('Initializing database');
    logger.debug(`Database settings: ${JSON.stringify(config.database)}`);
    const databaseSettings = new DatabaseSettings(
      config.database.host,
      config.database.user,
      config.database.password,
      config.database.database,
      config.database.port,
      config.database.connectionLimit
    );
    const database: Database = new Database(databaseSettings);
    const app = new Application(logger, config, database);
    await app.initialize();
  } catch (error:unknown) {
    if (error instanceof Error) {
      logger.error(error.message);
    } else {
      logger.error("Unknown Error")
    }
    
  }
}

main();