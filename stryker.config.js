// @ts-check
/** @type {import('@stryker-mutator/api/core').PartialStrykerOptions} */
module.exports = {
  packageManager: "npm",
  reporters: ["html", "clear-text", "progress"],
  ignoreStatic: true,
  testRunner: "jest",
  jest: {
    projectType: "custom",
    config: require('./jest.config.js'),
    enableFindRelatedTests: true,
  },
  coverageAnalysis: "perTest",
  tsconfigFile: "tsconfig.json",
  mutate: [
    "src/**/*.ts", 
    "!src/**/*.spec.ts",
    "!src/**/__mocks__/**/*.ts",
  ],
  checkers: ['typescript'],
};
