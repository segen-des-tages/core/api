<!--
&lt;--- Readme.md Snippet without images Start ---&gt;
## Tech Stack
segen-des-tages/api is built on the following main stack:

- [TypeScript](http://www.typescriptlang.org) – Languages
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) – Languages
- [ExpressJS](http://expressjs.com/) – Microframeworks (Backend)
- [Node.js](http://nodejs.org/) – Frameworks (Full Stack)
- [Cron](https://directory.fsf.org/wiki/Cron) – Background Processing
- [MySQL](http://www.mysql.com) – Databases
- [Docker](https://www.docker.com/) – Virtual Machine Platforms & Containers
- [ESLint](http://eslint.org/) – Code Review
- [GitHub Actions](https://github.com/features/actions) – Continuous Integration
- [Jest](http://facebook.github.io/jest/) – Javascript Testing Framework
- [SuperTest](https://www.npmjs.com/package/supertest) – Javascript Testing Framework
- [Shell](https://en.wikipedia.org/wiki/Shell_script) – Shells

Full tech stack [here](/techstack.md)

&lt;--- Readme.md Snippet without images End ---&gt;

&lt;--- Readme.md Snippet with images Start ---&gt;
## Tech Stack
segen-des-tages/api is built on the following main stack:

- <img width='25' height='25' src='https://img.stackshare.io/service/1612/bynNY5dJ.jpg' alt='TypeScript'/> [TypeScript](http://www.typescriptlang.org) – Languages
- <img width='25' height='25' src='https://img.stackshare.io/service/1209/javascript.jpeg' alt='JavaScript'/> [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) – Languages
- <img width='25' height='25' src='https://img.stackshare.io/service/1163/hashtag.png' alt='ExpressJS'/> [ExpressJS](http://expressjs.com/) – Microframeworks (Backend)
- <img width='25' height='25' src='https://img.stackshare.io/service/1011/n1JRsFeB_400x400.png' alt='Node.js'/> [Node.js](http://nodejs.org/) – Frameworks (Full Stack)
- <img width='25' height='25' src='https://img.stackshare.io/service/2416/default_274ed9bcc98502018f007cfcd8c09f3514d3d319.png' alt='Cron'/> [Cron](https://directory.fsf.org/wiki/Cron) – Background Processing
- <img width='25' height='25' src='https://img.stackshare.io/service/1025/logo-mysql-170x170.png' alt='MySQL'/> [MySQL](http://www.mysql.com) – Databases
- <img width='25' height='25' src='https://img.stackshare.io/service/586/n4u37v9t_400x400.png' alt='Docker'/> [Docker](https://www.docker.com/) – Virtual Machine Platforms & Containers
- <img width='25' height='25' src='https://img.stackshare.io/service/3337/Q4L7Jncy.jpg' alt='ESLint'/> [ESLint](http://eslint.org/) – Code Review
- <img width='25' height='25' src='https://img.stackshare.io/service/11563/actions.png' alt='GitHub Actions'/> [GitHub Actions](https://github.com/features/actions) – Continuous Integration
- <img width='25' height='25' src='https://img.stackshare.io/service/830/jest.png' alt='Jest'/> [Jest](http://facebook.github.io/jest/) – Javascript Testing Framework
- <img width='25' height='25' src='https://img.stackshare.io/no-img-open-source.png' alt='SuperTest'/> [SuperTest](https://www.npmjs.com/package/supertest) – Javascript Testing Framework
- <img width='25' height='25' src='https://img.stackshare.io/service/4631/default_c2062d40130562bdc836c13dbca02d318205a962.png' alt='Shell'/> [Shell](https://en.wikipedia.org/wiki/Shell_script) – Shells

Full tech stack [here](/techstack.md)

&lt;--- Readme.md Snippet with images End ---&gt;
-->
<div align="center">

# Tech Stack File
![](https://img.stackshare.io/repo.svg "repo") [segen-des-tages/api](https://github.com/segen-des-tages/api)![](https://img.stackshare.io/public_badge.svg "public")
<br/><br/>
|31<br/>Tools used|03/14/24 <br/>Report generated|
|------|------|
</div>

## <img src='https://img.stackshare.io/languages.svg'/> Languages (3)
<table><tr>
  <td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1612/bynNY5dJ.jpg' alt='TypeScript'>
  <br>
  <sub><a href="http://www.typescriptlang.org">TypeScript</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/6727/css.png' alt='CSS 3'>
  <br>
  <sub><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/CSS3">CSS 3</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1209/javascript.jpeg' alt='JavaScript'>
  <br>
  <sub><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript">JavaScript</a></sub>
  <br>
  <sub></sub>
</td>

</tr>
</table>

## <img src='https://img.stackshare.io/frameworks.svg'/> Frameworks (2)
<table><tr>
  <td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1163/hashtag.png' alt='ExpressJS'>
  <br>
  <sub><a href="http://expressjs.com/">ExpressJS</a></sub>
  <br>
  <sub>v4.18.3</sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1011/n1JRsFeB_400x400.png' alt='Node.js'>
  <br>
  <sub><a href="http://nodejs.org/">Node.js</a></sub>
  <br>
  <sub></sub>
</td>

</tr>
</table>

## <img src='https://img.stackshare.io/databases.svg'/> Data (2)
<table><tr>
  <td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/2416/default_274ed9bcc98502018f007cfcd8c09f3514d3d319.png' alt='Cron'>
  <br>
  <sub><a href="https://directory.fsf.org/wiki/Cron">Cron</a></sub>
  <br>
  <sub>v3.1.6</sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1025/logo-mysql-170x170.png' alt='MySQL'>
  <br>
  <sub><a href="http://www.mysql.com">MySQL</a></sub>
  <br>
  <sub></sub>
</td>

</tr>
</table>

## <img src='https://img.stackshare.io/devops.svg'/> DevOps (7)
<table><tr>
  <td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/586/n4u37v9t_400x400.png' alt='Docker'>
  <br>
  <sub><a href="https://www.docker.com/">Docker</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/3337/Q4L7Jncy.jpg' alt='ESLint'>
  <br>
  <sub><a href="http://eslint.org/">ESLint</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1046/git.png' alt='Git'>
  <br>
  <sub><a href="http://git-scm.com/">Git</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/11563/actions.png' alt='GitHub Actions'>
  <br>
  <sub><a href="https://github.com/features/actions">GitHub Actions</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/830/jest.png' alt='Jest'>
  <br>
  <sub><a href="http://facebook.github.io/jest/">Jest</a></sub>
  <br>
  <sub>v29.7.0</sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/no-img-open-source.png' alt='SuperTest'>
  <br>
  <sub><a href="https://www.npmjs.com/package/supertest">SuperTest</a></sub>
  <br>
  <sub>v6.3.4</sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1120/lejvzrnlpb308aftn31u.png' alt='npm'>
  <br>
  <sub><a href="https://www.npmjs.com/">npm</a></sub>
  <br>
  <sub></sub>
</td>

</tr>
</table>

## Other (2)
<table><tr>
  <td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/4631/default_c2062d40130562bdc836c13dbca02d318205a962.png' alt='Shell'>
  <br>
  <sub><a href="https://en.wikipedia.org/wiki/Shell_script">Shell</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/9527/5502029.jpeg' alt='husky'>
  <br>
  <sub><a href="https://github.com/typicode/husky">husky</a></sub>
  <br>
  <sub></sub>
</td>

</tr>
</table>


## <img src='https://img.stackshare.io/group.svg' /> Open source packages (15)</h2>

## <img width='24' height='24' src='https://img.stackshare.io/service/1120/lejvzrnlpb308aftn31u.png'/> npm (15)

|NAME|VERSION|LAST UPDATED|LAST UPDATED BY|LICENSE|VULNERABILITIES|
|:------|:------|:------|:------|:------|:------|
|[@types/cors](https://www.npmjs.com/@types/cors)|v2.8.17|03/05/24|dependabot[bot] |MIT|N/A|
|[@types/express](https://www.npmjs.com/@types/express)|v4.17.21|03/05/24|dependabot[bot] |MIT|N/A|
|[@types/jest](https://www.npmjs.com/@types/jest)|v29.5.12|03/05/24|dependabot[bot] |MIT|N/A|
|[@types/node](https://www.npmjs.com/@types/node)|v20.11.24|03/05/24|dependabot[bot] |MIT|N/A|
|[@types/supertest](https://www.npmjs.com/@types/supertest)|v6.0.2|03/05/24|dependabot[bot] |MIT|N/A|
|[@typescript-eslint/eslint-plugin](https://www.npmjs.com/@typescript-eslint/eslint-plugin)|v7.1.0|03/05/24|dependabot[bot] |MIT|N/A|
|[@typescript-eslint/parser](https://www.npmjs.com/@typescript-eslint/parser)|v7.1.1|03/05/24|dependabot[bot] |BSD-2-Clause|N/A|
|[cors](https://www.npmjs.com/cors)|v2.8.5|02/24/24|Dominik.Sigmund |MIT|N/A|
|[eslint-plugin-security](https://www.npmjs.com/eslint-plugin-security)|v2.1.1|03/05/24|dependabot[bot] |Apache-2.0|N/A|
|[express](https://www.npmjs.com/express)|v4.18.3|03/04/24|Dominik.Sigmund |MIT|N/A|
|[husky](https://www.npmjs.com/husky)|v9.0.11|03/04/24|Dominik.Sigmund |MIT|N/A|
|[mysql2](https://www.npmjs.com/mysql2)|v3.9.2|03/04/24|Dominik.Sigmund |MIT|N/A|
|[ts-jest](https://www.npmjs.com/ts-jest)|v29.1.2|03/04/24|Dominik.Sigmund |MIT|N/A|
|[ts-node](https://www.npmjs.com/ts-node)|v10.9.2|03/04/24|Dominik.Sigmund |MIT|N/A|
|[typedoc](https://www.npmjs.com/typedoc)|v0.25.10|03/05/24|dependabot[bot] |Apache-2.0|N/A|

<br/>
<div align='center'>

Generated via [Stack File](https://github.com/marketplace/stack-file)
